package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.SupplyCurrentLimitConfiguration;
import com.ctre.phoenix.motorcontrol.can.TalonSRX;
import frc.robot.RobotMap;

public class Hopper {

    private static final double ROTOR_HI_POWER = 1.0,
            ROTOR_LOW_POWER = -ROTOR_HI_POWER,
            ROLLER_HI_POWER = 1.0,
            ROLLER_LOW_POWER = -ROLLER_HI_POWER,
            ROTOR_PO = 0.70,
            ROLLER_PO = 1.0;
    //Speed Controllers, these are the devices that control the power of the motors.
    private TalonSRX rotor, rollers;

    public Hopper() {
        rotor = new TalonSRX(RobotMap.TOP_HOPPER_MOTOR);
        rollers = new TalonSRX(RobotMap.SPIN_FEEDER_MOTOR);
        //Configuring the minimum power and maximum power that can be supplied to each motor. Just a bounds thing.
        this.rotor.configPeakOutputForward(ROTOR_HI_POWER, 0);
        this.rotor.configPeakOutputReverse(ROTOR_LOW_POWER, 0);
        this.rotor.configNominalOutputForward(0.0, 0);
        this.rotor.configNominalOutputReverse(0.0, 0);
        this.rollers.configPeakOutputForward(ROLLER_HI_POWER, 0);
        this.rollers.configPeakOutputReverse(ROLLER_LOW_POWER, 0);
        this.rollers.configNominalOutputForward(0.0, 0);
        this.rollers.configNominalOutputReverse(0.0, 0);

        this.rollers.configContinuousCurrentLimit(30);
        this.rollers.configSupplyCurrentLimit(new SupplyCurrentLimitConfiguration(true, 30, 5, 2));
        this.rotor.configContinuousCurrentLimit(30);
        this.rotor.configSupplyCurrentLimit(new SupplyCurrentLimitConfiguration(true, 30, 5, 2));
        this.rollers.enableCurrentLimit(true);
        this.rotor.enableCurrentLimit(true);

        //This is here for quick inversion of either one of the systems
        this.rotor.setInverted(false);
        this.rollers.setInverted(false);
    }

    public void feed() {
        this.rotor.set(ControlMode.PercentOutput, ROTOR_PO);

    }

    public void roll() {
        this.rollers.set(ControlMode.PercentOutput, ROLLER_PO);

    }

    public void stopFeed() {
        this.rotor.set(ControlMode.PercentOutput, 0.0);

    }

    public void stopRoll() {
        this.rollers.set(ControlMode.PercentOutput, 0.0);
    }
}
