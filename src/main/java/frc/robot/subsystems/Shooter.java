package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.FeedbackDevice;
import com.ctre.phoenix.motorcontrol.can.TalonSRX;
import frc.robot.RobotMap;

public class Shooter {
    private static final double SHOOTER_HI_POWER = 1.0,
            SHOOTER_LOW_POWER = -SHOOTER_HI_POWER,
            PVAL = 1.0,
            IVAL = 0.0,
            DVAL = 3.0,
            FVAL = 0.035;
    //Speed Controllers, these are the devices that control the power of the motors.
    private TalonSRX shooterMain;
    private double velocity = Velocities.lowPow.getVelocity();

    public Shooter() {
        //Follower Controllers setup, telling the followers to mimic their leaders.
        shooterMain = new TalonSRX(RobotMap.SHOOTER_MOTOR_1);
        TalonSRX shooterFollower = new TalonSRX(RobotMap.SHOOTER_MOTOR_2);
        shooterFollower.follow(shooterMain);

        //Configuring the minimum power and maximum power that can be supplied to each motor. Just a bounds thing.
        this.shooterMain.configPeakOutputForward(SHOOTER_HI_POWER, 0);
        this.shooterMain.configPeakOutputReverse(SHOOTER_LOW_POWER, 0);
        this.shooterMain.configNominalOutputForward(0.0, 0);
        this.shooterMain.configNominalOutputReverse(0.0, 0);
        shooterFollower.configPeakOutputForward(SHOOTER_HI_POWER, 0);
        shooterFollower.configPeakOutputReverse(SHOOTER_LOW_POWER, 0);
        shooterFollower.configNominalOutputForward(0.0, 0);
        shooterFollower.configNominalOutputReverse(0.0, 0);

        //Setting one of the sides of the shooter to be inverted or not could be important
        this.shooterMain.setInverted(false);
        shooterFollower.setInverted(false);

        //Encoder
        this.shooterMain.configSelectedFeedbackSensor(FeedbackDevice.CTRE_MagEncoder_Relative, 0, 0);
        this.shooterMain.config_kP(0, PVAL, 0);
        this.shooterMain.config_kI(0, IVAL, 0);
        this.shooterMain.config_kD(0, DVAL, 0);
        this.shooterMain.config_kF(0, FVAL, 0);
    }

    public void shoot(double pow) {
        this.shooterMain.set(ControlMode.PercentOutput, pow);
    }

    public double getShooterVel() {
        return shooterMain.getSelectedSensorVelocity(0);
    }

    public void changeShooterVel(Velocities vel) {
        velocity = vel.getVelocity();
    }

    //1u velocity represents 1/4096 of a rotation every 100ms
    public void shootAtVelocity() {
        this.shooterMain.set(ControlMode.Velocity, velocity);
    }

    public double getDesiredVel() {
        return velocity;
    }

    public void stopShooter() {
        this.shooterMain.set(ControlMode.PercentOutput, 0.0);
    }

    public enum Velocities {
        veryLowPow(204800d / 50),
        lowPow(409600d / 50),
        mediumPow(614400d / 50),
        highPow(819200d / 50);
        private double velocity;

        Velocities(double velocity) {
            this.velocity = velocity;
        }

        public double getVelocity() {
            return this.velocity;
        }
    }
}
