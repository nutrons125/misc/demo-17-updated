package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.can.TalonSRX;
import frc.robot.RobotMap;

public class Turret {
    private static final double HOOD_HI_POWER = 1.0,
            HOOD_LOW_POWER = -HOOD_HI_POWER;
    //Speed Controllers, these are the devices that control the power of the motors.
    private TalonSRX hood;

    public Turret() {
        hood = new TalonSRX(RobotMap.HOOD_MOTOR_A);
        //Configuring the minimum power and maximum power that can be supplied to each motor. Just a bounds thing.
        this.hood.configPeakOutputForward(HOOD_HI_POWER, 0);
        this.hood.configPeakOutputReverse(HOOD_LOW_POWER, 0);
        this.hood.configNominalOutputForward(0.0, 0);
        this.hood.configNominalOutputReverse(0.0, 0);

        enableBreakMode();

        //This is here for quick inversion of the turret
        this.hood.setInverted(false);
    }

    //Enables break mode in neutral, where neutral is no power supplied.
    public void enableBreakMode() {
        this.hood.setNeutralMode(NeutralMode.Brake);
    }

    //Disables break mode in neutral, where neutral is no power supplied.
    @SuppressWarnings("unused")
    public void disableBreakMode() {
        this.hood.setNeutralMode(NeutralMode.Coast);
    }

    public void turnHood(double pow) {
        this.hood.set(ControlMode.PercentOutput, pow);
    }
}
