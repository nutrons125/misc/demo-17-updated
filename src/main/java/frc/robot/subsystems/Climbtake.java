package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.SupplyCurrentLimitConfiguration;
import com.ctre.phoenix.motorcontrol.can.TalonSRX;
import frc.robot.RobotMap;

public class Climbtake {

    private static final double CLIMBTAKE_HI_POWER = 1.0,
            CLIMBTAKE_LOW_POWER = -CLIMBTAKE_HI_POWER,
            CLIMBTAKE_PO = 1.0;
    //Speed Controllers, these are the devices that control the power of the motors.
    private TalonSRX climbtakeMain;

    public Climbtake() {
        //Follower Controllers setup, telling the followers to mimic their leaders.
        climbtakeMain = new TalonSRX(RobotMap.CLIMBTAKE_MOTOR_1);
        TalonSRX climbtakeFollower = new TalonSRX(RobotMap.CLIMBTAKE_MOTOR_2);
        climbtakeFollower.follow(climbtakeMain);

        //Configuring the minimum power and maximum power that can be supplied to each motor. Just a bounds thing.
        this.climbtakeMain.configPeakOutputForward(CLIMBTAKE_HI_POWER, 0);
        this.climbtakeMain.configPeakOutputReverse(CLIMBTAKE_LOW_POWER, 0);
        this.climbtakeMain.configNominalOutputForward(0.0, 0);
        this.climbtakeMain.configNominalOutputReverse(0.0, 0);
        climbtakeFollower.configPeakOutputForward(CLIMBTAKE_HI_POWER, 0);
        climbtakeFollower.configPeakOutputReverse(CLIMBTAKE_LOW_POWER, 0);
        climbtakeFollower.configNominalOutputForward(0.0, 0);
        climbtakeFollower.configNominalOutputReverse(0.0, 0);

        this.climbtakeMain.configContinuousCurrentLimit(40);
        this.climbtakeMain.configSupplyCurrentLimit(new SupplyCurrentLimitConfiguration(true, 40, 5, 2));
        this.climbtakeMain.enableCurrentLimit(true);


        //Setting one of the sides of the climbtake to be inverted or not could be important
        this.climbtakeMain.setInverted(false);
        climbtakeFollower.setInverted(false);
    }

    public void climbtake() {
        this.climbtakeMain.set(ControlMode.PercentOutput, CLIMBTAKE_PO);
    }

    public void stopClimbtake() {
        this.climbtakeMain.set(ControlMode.PercentOutput, 0.0);
    }
}

