/*----------------------------------------------------------------------------*/
/* Copyright (c) 2017-2018 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;

import edu.wpi.first.wpilibj.TimedRobot;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.robot.subsystems.*;
import frc.robot.utils.Controller;

/**
 * The robot class is responsible for running the robot
 * during matches. It handles everything from creating auto commands and subsystems (including the OI)
 * and deciding what to do as the robot switches between modes. It is the highest level of control that
 * you should be concerned with.
 */
public class Robot extends TimedRobot {
    public static Drivetrain drivetrain = new Drivetrain();
    public static Climbtake climbtake = new Climbtake();
    public static Hopper hopper = new Hopper();
    public static Shooter shooter = new Shooter();
    public static Turret turret = new Turret();
    public static OI oi;
    Controller driverController, operatorController;

    //robotInit is ran when the robot is first turned on
    @Override
    public void robotInit() {
        oi = new OI();
        driverController = oi.getDriverController();
        operatorController = oi.getOperatorController();
        driverController.setLeftStickXDeaband(0.05);
        operatorController.setLeftStickXDeaband(0.05);
    }

    @Override
    public void robotPeriodic() {
        updateSmartDash();
    }

    //teleopInit is ran when the robot enters teleop mode
    @Override
    public void teleopInit() {
    }

    //teleopPeriodic is ran repeatedly while the robot is in teleop mode
    @Override
    public void teleopPeriodic() {
        drivetrain.driveArcade(oi.getDriverController().getTriggerSum(), oi.getDriverController().getLeftStickX());

        //Climbtake
        if (driverController.getRightBumper()) {
            climbtake.climbtake();
        } else {
            climbtake.stopClimbtake();
        }

        //Hopper
        if (operatorController.getRightBumper()) {
            hopper.feed();
            hopper.roll();
        } else {
            hopper.stopFeed();
            hopper.stopRoll();
        }

        //Shooter
        if (operatorController.a()) {
            shooter.changeShooterVel(Shooter.Velocities.veryLowPow);
        } else if (operatorController.x()) {
            shooter.changeShooterVel(Shooter.Velocities.lowPow);
        } else if (operatorController.y()) {
            shooter.changeShooterVel(Shooter.Velocities.mediumPow);
        } else if (operatorController.b()) {
            shooter.changeShooterVel(Shooter.Velocities.highPow);
        }

        if (operatorController.getLeftBumper()) {
            shooter.shootAtVelocity();
        } else {
            shooter.shoot(operatorController.getRightTrigger());
        }

        //Turret
        turret.turnHood(operatorController.getLeftStickX());
    }

    //disabledInit is ran when the robot enters a disabled state
    @Override
    public void disabledInit() {
        shooter.stopShooter();
    }

    //disabledPeriodic is ran repeatedly while the robot is in a disabled state
    @Override
    public void disabledPeriodic() {
    }

    public void updateSmartDash() {
        SmartDashboard.putNumber("Shooter Velocity", shooter.getShooterVel());
        SmartDashboard.putNumber("Current Velocity Setpoint", shooter.getDesiredVel());
    }
}
